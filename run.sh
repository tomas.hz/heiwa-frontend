#!/bin/bash

##
# Runs the Heiwa Frontend app in a Gunicorn server, optimized for a 4-core system - (4 cores * 2) + 1.
# Gunicorn documentation: https://docs.gunicorn.org/en/latest/index.html
# Flask documentation, more options and information: https://flask.palletsprojects.com/en/2.0.x/patterns/appfactories/
##

gunicorn -w 9 -b :5002 heiwa_frontend:"create_app()"
