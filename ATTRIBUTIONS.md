# Open Source License Attributions

## [Roboto](https://github.com/googlefonts/roboto)
Licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).

Copyright 2011 Google Inc. All Rights Reserved.

## [Roboto Mono](https://github.com/googlefonts/RobotoMono)
Licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).

Copyright 2015 The Roboto Mono Project Authors.
