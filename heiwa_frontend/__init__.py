from __future__ import annotations

import datetime
import json
import logging
import os

import flask
import flask_babel
import flask_executor
import requests_cache
import werkzeug.exceptions

from . import utils

__all__ = ["create_app"]
__version__ = "0.0.29"

logging.basicConfig(
	format="[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
	level=getattr(
		logging,
		os.environ.get(
			"LOGGING_LEVEL",
			"DEBUG"
		)
	)
)


def create_app() -> flask.Flask:
	"""Creates a pre-configured :class:`Flask <flask.Flask>` app."""

	app = flask.Flask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		app.logger.debug("Loading config file")

		app.config.from_file(
			os.environ.get(
				"CONFIG_LOCATION",
				os.path.join(
					os.getcwd(),
					"config.json"
				)
			),
			load=json.load
		)

		executor = flask_executor.Executor(app)
		app.executor = executor

		babel = flask_babel.Babel(app)
		app.babel = babel

		@app.before_request
		def before_request() -> None:
			flask.g.current_time = datetime.datetime.now(tz=datetime.timezone.utc)

			flask.g.session = requests_cache.CachedSession(
				os.environ.get(
					"CACHE_NAME",
					"cache.sqlite"
				),
				backend="sqlite",
				expire_after=app.config["CACHE_EXPIRES_AFTER"],
				match_headers=("Authorization",),
				allowable_codes=(200, 201, 204),
				allowable_methods=("GET",)
			)

		@app.teardown_request
		def teardown_request(response: flask.Response) -> flask.Response:
			if "session" in flask.g:
				flask.g.session.close()

			return response

		@app.template_filter()
		def isotime_difference(time: str) -> str:
			return flask_babel.gettext(
				"%(timedelta)s ago",
				timedelta=flask_babel.format_timedelta(
					flask.g.current_time
					- datetime.datetime.fromisoformat(time)
				)
			)

		@app.template_filter()
		def human_number(number: int, round_to: int = 1) -> str:
			# https://stackoverflow.com/a/49955617

			magnitude = 0

			while abs(number) >= 1000:
				magnitude += 1
				number = round(number / 1000.0, round_to)

			if magnitude == 0:
				return str(number)

			return '{:.{}f}{}'.format(
				number,
				round_to if magnitude != 0 else 0,  # Don't do "0.0" etc.
				[
					'',
					flask_babel.gettext('K'),
					flask_babel.gettext('M')
				][magnitude]
			)

		@app.route("/")
		def index() -> flask.Response:
			return flask.redirect(
				flask.url_for(flask.current_app.config["BASE_VIEW"])
			)

		@app.errorhandler(werkzeug.exceptions.HTTPException)
		@utils.full_view(anonymous=True)
		def handle_http_exception(
			exception: werkzeug.exceptions.HTTPException
		) -> typing.Union[flask.Response, int]:
			return flask.render_template(
				"error.html",
				exception=exception
			), exception.code

		from .views import (
			file_blueprint,
			forum_blueprint,
			message_blueprint,
			meta_blueprint,
			account_blueprint,
			statistic_blueprint,
			tos_blueprint,
			user_blueprint
		)

		for blueprint in (
			file_blueprint,
			forum_blueprint,
			message_blueprint,
			meta_blueprint,
			account_blueprint,
			statistic_blueprint,
			tos_blueprint,
			user_blueprint
		):
			app.logger.debug("Registering blueprint: %s", blueprint)

			app.register_blueprint(blueprint)

	return app
