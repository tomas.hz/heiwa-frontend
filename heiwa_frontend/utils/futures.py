import typing
import concurrent.futures

__all__ = ["handle_futures"]


def handle_futures(
	futures: typing.Iterable[concurrent.futures.Future]
) -> None:
	concurrent.futures.wait(
		futures,
		return_when=concurrent.futures.FIRST_EXCEPTION
	)

	for future in futures:
		exception = future.exception()

		if exception is not None:
			raise exception
