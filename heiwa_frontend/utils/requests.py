import typing

import flask
import requests

from .. import exceptions


def request(
	session: requests.Session,
	method: str,
	endpoint: str,
	headers: typing.Union[
		None,
		typing.Dict[str, typing.Any]
	] = None,
	ignore_token: bool = False,
	**kwargs
) -> typing.Any:  # TODO: JSON type hint
	# https://stackoverflow.com/q/26320899
	# Oh.

	if headers is None:
		headers = {}

	if "Authorization" not in headers:
		if (
			not ignore_token
			and "token" in flask.g
		):
			headers["Authorization"] = f"Bearer {flask.g.token}"

	flask.current_app.logger.debug(
		"Sending %s request to %s - Headers: %s, keyword arguments: %s",
		method,
		endpoint,
		headers,
		kwargs
	)

	with session.request(
		method,
		f"{flask.current_app.config['API_URL']}/{endpoint}",
		headers=headers,
		**kwargs
	) as response:
		if not response.ok:
			# We can consume the JSON here, since we're not returning the result

			raise exceptions.BackendException(
				*response.json().values(),
				code=response.status_code
			)

		return response
