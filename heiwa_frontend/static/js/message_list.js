var privateKey = window.localStorage.getItem("privateKey");
var currentUser = null;

/*
 * Shows basic messaging elements. This is to be run once one of the private
 * key entry functions is complete.
 */
function showMessageElements() {
	document.getElementById("new-message-wrapper").style.display = "block";
	document.getElementById("message-window-wrapper").style.display = "grid";
}

/*
 * Set the browser's stored private key to the element ID
 * `manual-privkey-input`'s value.
 */
function getPrivateKey() {
	// TODO: Validate input
	window.localStorage.setItem(
		"privateKey",
		document.getElementById("manual-privkey-input").value
	);
	
	document.getElementById("manual-privkey-input-wrapper").style.display = "none";
	showMessageElements();
}

/*
 * Manually decrypt the current user's encrypted private key using the password
 * given in the element ID `privkey-password-input`. It's automatically padded
 * to be 16, 24 or 32 characters depending on the lowest possible length. If it
 * is over 32 characters, the extra characters are removed.
 */
function decryptPrivateKey() {
	let utf8Encoder = new TextEncoder();
	
	var passphrase = document.getElementById("privkey-password-input").value;
	
	if (
		passphrase.length !== 32 &&
		passphrase.length !== 24 &&
		passphrase.length !== 16
	) {
		if (passphrase.length < 16) {
			passphrase += " ".repeat(16 - passphrase.length);
		} else if (passphrase.length < 24) {
			passphrase += " ".repeat(24 - passphrase.length);
		} else if (passphrase.length < 32) {
			passphrase += " ".repeat(32 - passphrase.length);
		} else {
			passphrase = passphrase.slice(0, 32);
		}
	}
	
	let decipher = forge.cipher.createDecipher(
		"AES-CBC",
		forge.util.createBuffer(
			utf8Encoder.encode(passphrase)
		)
	);
	
	decipher.start(
		{
			iv: forge.util.createBuffer(
				atob(currentUser.encrypted_private_key_iv)
			)
		}
	);
	
	decipher.update(
		forge.util.createBuffer(
			atob(currentUser.encrypted_private_key)
		)
	);
	
	const resultIsOk = decipher.finish();
	
	if (!resultIsOk) {
		// Make this nicer later
		alert(gettext("Could not decrypt private key."));
		
		return;
	} else {
		privateKey = (
			"-----BEGIN PRIVATE KEY-----\n"
			+ btoa(decipher.output.bytes())
			+ "\n-----END PRIVATE KEY-----"
		)
		
		window.localStorage.setItem("privateKey", privateKey);
		
		document.getElementById("manual-privkey-input-wrapper").style.display = "none";
		showMessageElements();
	}
}

/*
 * Handles whether or not the private key is set.
 * 
 * If `privateKey` is `null` (nothing is in the local storage), gets the
 * current user and sees if they have a set encrypted private key. If they do,
 * they're prompted to enter a password. Otherwise, they're prompted to enter a
 * private key.
 * 
 * This function is meant to be run when the browser window loads.
 */
async function loadUserAndHandlePrivateKeyExistence() {
	const currentUserErrorMessage = gettext("Could not get the current user. Please contact the administrator about this issue.");
	
	try {
		currentUser = await (
			fetch(
				`${API_URL}/self`,
				{
					"headers": {
						"Authorization": generateFetchAuthorizationHeader()
					}
				}
			).
			then(response => {
				if (!response.ok) {
					alert(currentUserErrorMessage);
					
					throw new Error(
						"Failed to fetch the current user. The response code was: "
						+ response.code
					);
				}
				
				return response.json();
			})
		);
	} catch (exception) {
		console.log(
			"Current user fetch error: ",
			exception
		);
		alert(currentUserErrorMessage);

		return;
	}
	
	if (privateKey === null) {
		if (currentUser.encrypted_private_key === null) {
			document.getElementById("manual-privkey-input-wrapper").style.display = "block";
		} else {
			document.getElementById("privkey-decrypt-wrapper").style.display = "block";
		}
	} else {
		showMessageElements();
	}
}

/*
 * Selects or unselects the given directory.
 * 
 * If its ``isSelected`` dataset attribute is ``"false"``, it's set to
 * ``"true"`` and the ``message-directory-selected`` CSS class is added. All
 * other selected directories are deselected and the directory's contents are
 * loaded.
 * 
 * If the initial ``isSelected`` attribute is ``"true"``, the directory is
 * deselected and the ``showNoMessages()`` function is run.
 */
async function selectDirectory(directory, allDirectoryElements) {
	if (directory.dataset.isSelected === "false") {
		for (const element of allDirectoryElements) {
			if (!element.dataset.isSelected) {
				continue;
			}

			element.dataset.isSelected = "false";
			element.classList.remove("message-directory-selected");
		}

		directory.dataset.isSelected = "true";
		directory.classList.add("message-directory-selected");
	} else {
		directory.dataset.isSelected = "false";
		directory.classList.remove("message-directory-selected");
		
		showNoMessages();
		
		return;
	}

	if (directory.dataset.isDefault) {
		await listMessagesInDirectory(directory.dataset.defaultType, isType=true);
	} else {
		await listMessagesInDirectory(directory.id);
	}
}

/*
 * Sets up interface elements.
 * 
 * Adds ``click`` event listeners to all elements of the ``message-directory``
 * class, enabling the user to select them and list the messages contained
 * inside.
 * 
 * The first default directory is always opened on the first start.
 */
function loadInterface() {
	const directoryElements = document.getElementsByClassName("message-directory");

	let openedFirstDefaultDir = false;

	for (const element of directoryElements) {
		element.addEventListener(
			"click",
			function(event) { selectDirectory(event.currentTarget, directoryElements); }
		);

		if (
			!openedFirstDefaultDir &&
			element.dataset.isDefault
		) {
			openedFirstDefaultDir = true;
			selectDirectory(element, directoryElements);
		}
	}

	document.getElementById("manual-private-key-set").addEventListener(
		"click",
		getPrivateKey
	);

	document.getElementById("privkey-decrypt").addEventListener(
		"click",
		decryptPrivateKey
	);
	document.getElementById("privkey-password-input").addEventListener(
		"keypress",
		function(event) {
			if (event.key === "Enter") {
				event.preventDefault();
				decryptPrivateKey();
			}
		}
	)
}

async function listMessagesInDirectory(directoryIdentifier, isType = false) {
	const messageListErrorMessage = gettext("Could not get the list of messages. Please contact the administrator about this issue.");
	
	let messages = null;
	
	try {
		messages = await (
			fetch(
				`${API_URL}/messages`,
				{
					"method": "QUERY",
					"headers": {
						"Authorization": generateFetchAuthorizationHeader(),
						"Content-Type": "application/json"
					},
					"body": JSON.stringify({
						"limit": 50,
						"filter": (
							(isType) ?
							(
								(directoryIdentifier === "received") ?
								{
									"$and": [
										{"$equals": {"sender_directory_id": null}},
										{"$equals": {"sender_id": currentUser.id}}
									]
								} :
								{
									"$and": [
										{"$equals": {"sender_directory_id": null}},
										{"$equals": {"receiver_id": currentUser.id}}
									]
								}
							) :
							{"$equals": {"sender_directory_id": directoryIdentifier}}
						)
					})
				}
			).
			then(response => {
				if (!response.ok) {
					alert(messageListErrorMessage);
					
					throw new Error(
						"Failed to fetch messages. The response code was: "
						+ response.code
					);
				}
				
				return response.json();
			})
		);
	} catch (exception) {
		console.log(
			"Message list fetch error: ",
			exception
		);
		alert(messageListErrorMessage);

		return;
	}

	if (messages.length === 0) {
		showNoMessages();

		return;
	}

	const messageViewElement = document.getElementById("message-view-wrapper");

	const messageListElement = document.createElement("ul");
	messageListElement.classList.add("message-list");

	messageViewElement.innerHTML = "";  // Clear all child elements
	messageViewElement.appendChild(messageListElement);  // Add the list as the only child

	for (const message of messages) {
		// Message container
		const messageElement = document.createElement("li");
		messageElement.classList.append("message");

		// Subject
		const messageSubjectLinkElement = document.createElement("a");
		messageSubjectLinkElement.classList.append("message-subject");
		messageSubjectLinkElement.addEventListener(
			"click",
			function() {
				// TODO
			}
		);

		const messageSubjectNameElement = document.createElement("h3");
		messageSubjectNameElement.innerHTML = message.subject;

		messageSubjectLinkElement.appendChild(messageSubjectNameElement);

		messageElement.appendChild(messageSubjectLinkElement);

		// Time
		const messageTimeElement = document.createElement("div");
		messageTimeElement.classList.append("message-creation-timestamp");

		const creationTimestamp = new Date(message.creation_timestamp);

		messageTimeElement.innerHTML = creationTimestamp.toLocaleString();

		messageElement.appendChild(messageTimeElement);

		// Add to the list
		messageListElement.appendChild(messageElement);
	}
}

/* 
 * Tells the user that there are no messages available, usually due to no
 * directory being selected.
 */
function showNoMessages() {
	const messageViewElement = document.getElementById("message-view-wrapper");
	messageViewElement.innerHTML = "";  // Clear any leftover elements

	const noMessagesText = document.createElement("p");
	noMessagesText.classList.add("message-view-no-messages");
	noMessagesText.classList.add("text-secondary");
	noMessagesText.innerHTML = gettext("No messages.");

	messageViewElement.appendChild(noMessagesText);
}

window.addEventListener(
	"DOMContentLoaded",
	async function() {
		await loadUserAndHandlePrivateKeyExistence();
		await loadInterface();
	}
);
