import base64
import email
import http.client
import typing
import uuid

import flask
import flask_babel
import flask_wtf
import flask_wtf.file
import werkzeug.utils
import wtforms

from heiwa_frontend import utils

__all__ = [
	"create",
	"file_blueprint",
	"view"
]


file_blueprint = flask.Blueprint(
	"file",
	__name__,
	url_prefix="/files"
)


class FileUploadForm(flask_wtf.FlaskForm):
	file_ = flask_wtf.file.FileField(
		validators=[
			flask_wtf.file.FileRequired()
		]
	)
	submit = wtforms.SubmitField(
		flask_babel.lazy_gettext("Upload")
	)


@file_blueprint.route("", methods=["GET", "POST"])
@utils.authentication_required
def create() -> typing.Union[
	flask.Response,
	typing.Tuple[flask.Response, int]
]:
	form = FileUploadForm()

	if form.validate_on_submit():
		with flask.g.session.cache_disabled():
			response = utils.request(
				flask.g.session,
				"post",
				"files",
				json={
					"name": werkzeug.utils.secure_filename(
						form.file_.data.filename
					),
					"contents": base64.b64encode(
						form.file_.data.read()
					).decode("utf-8")
				}
			).json()

		return flask.redirect(
			flask.url_for(
				"file.view",
				id_=response["id"]
			)
		)
	else:
		return flask.render_template(
			"file_create.html",
			form=form
		), http.client.OK


@file_blueprint.route("/<uuid:id_>", methods=["GET"])
@utils.authentication_required
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	response = utils.request(
		flask.g.session,
		"get",
		f"files/{id_}/contents",
		stream=True
	)

	return flask.send_file(
		response.raw,
		mimetype=response.headers["Content-Type"],
		as_attachment=True,
		download_name=response.headers["Content-Disposition"].split("filename=")[1],
		last_modified=email.utils.parsedate_to_datetime(
			response.headers["Last-Modified"]
		).timestamp()
	), http.client.OK
