import base64
import binascii
import http.client
import json
import typing
import uuid

import flask
import flask_babel
import flask_wtf
import wtforms
import wtforms.validators

from heiwa_frontend import utils

__all__ = [
	"MessageForm",
	"create",
	"message_blueprint"
]


message_blueprint = flask.Blueprint(
	"message",
	__name__,
	url_prefix="/messages"
)


def validate_is_base64(
	form: flask_wtf.FlaskForm,
	field: wtforms.Field
) -> None:
	try:
		base64.b64decode(
			field.data,
			validate=True
		)
	except binascii.Error as exception:
		raise wtforms.validators.ValidationError from exception


def validate_is_uuid(
	form: flask_wtf.FlaskForm,
	field: wtforms.Field
) -> None:
	try:
		uuid.UUID(field.data)
	except ValueError as exception:
		raise wtforms.validators.ValidationError from exception


class MessageForm(flask_wtf.FlaskForm):
	receiver_id = wtforms.HiddenField(
		validators=[
			wtforms.validators.InputRequired(),
			validate_is_uuid
		]
	)

	directory = wtforms.SelectField(
		flask_babel.lazy_gettext("Directory")
	)

	encrypted_session_key = wtforms.HiddenField(
		validators=[
			wtforms.validators.InputRequired(),
			validate_is_base64
		]
	)

	iv = wtforms.HiddenField(
		validators=[
			wtforms.validators.InputRequired(),
			validate_is_base64
		]
	)

	tag = wtforms.HiddenField(
		validators=[
			wtforms.validators.InputRequired(),
			validate_is_base64
		]
	)

	encrypted_content = wtforms.HiddenField(
		validators=[
			wtforms.validators.InputRequired(),
			validate_is_base64
		]
	)


@message_blueprint.route("/", methods=["GET"])
@utils.authentication_required
@utils.full_view()
def list_() -> typing.Tuple[flask.Response, int]:
	directories = (
		# These ones first!
		[
			{"name": flask_babel.gettext("Received"), "$type": "received"},
			{"name": flask_babel.gettext("Sent"), "$type": "sent"}
		]
		+ utils.request(
			flask.g.session,
			"query",
			"messages/directories",
			json={
				"limit": 512,
				"order": {
					"by": "order",
					"asc": False
				}
			}
		).json()
	)

	return flask.render_template(
		"message_list.html",
		directories=directories
	), http.client.OK


@message_blueprint.route("/create", methods=["GET", "POST"])
@utils.authentication_required
@utils.full_view()
def create() -> typing.Union[
	flask.Response,
	typing.Tuple[flask.Response, int]
]:
	form = MessageForm()

	directories = utils.request(
		flask.g.session,
		"query",
		"messages/directories",
		json={
			"limit": 512,
			"order": {
				"by": "order",
				"asc": False
			}
		}
	).json()

	choices = [
		(directory["id"], directory["name"])
		for directory in directories
	]

	choices.append(
		("", flask_babel.gettext("Sent (default directory)"))
	)

	form.directory.choices = choices

	if form.validate_on_submit():
		utils.request(
			flask.g.session,
			"post",
			"messages",
			json={
				"receiver_id": form.receiver_id.data,
				"sender_directory_id": (
					form.directory.data
					if form.directory.data != ""
					else None
				),
				"encrypted_session_key": form.encrypted_session_key.data,
				"iv": form.iv.data,
				"tag": form.tag.data,
				"encrypted_content": form.encrypted_content.data
			}
		)

		return "TODO (success, no errors!)"
	else:
		return flask.render_template(
			"message_create.html",
			form=form,
			json_current_user=json.dumps(flask.g.user)
		), http.client.OK
