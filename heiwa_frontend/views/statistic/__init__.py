import csv
import datetime
import http.client
import io
import os
import typing

import flask
import flask_babel
import matplotlib.pyplot
import PIL.Image
import PIL.ImageDraw
import PIL.ImageFont

from heiwa_frontend import utils

__all__ = [
	"statistic_blueprint",
	"transform_json_with_time_conditions",
	"view_image",
	"view_csv"
]


statistic_blueprint = flask.Blueprint(
	"statistic",
	__name__,
	url_prefix="/statistics"
)


def transform_json_with_time_conditions(json: typing.Dict) -> None:
	conditions = []

	if "from" in flask.request.args:
		try:
			min_creation_timestamp = datetime.datetime.fromisoformat(
				flask.request.args["from"]
			)

			if min_creation_timestamp.tzinfo is None:
				raise ValueError
		except ValueError:
			pass

		conditions.append({
			"$greater_than_or_equal_to": {
				"creation_timestamp": min_creation_timestamp.isoformat()
			}
		})

	if "until" in flask.request.args:
		try:
			max_creation_timestamp = datetime.datetime.fromisoformat(
				flask.request.args["until"]
			)

			if max_creation_timestamp.tzinfo is None:
				raise ValueError
		except ValueError:
			pass

		conditions.append({
			"$less_than_or_equal_to": {
				"creation_timestamp": max_creation_timestamp.isoformat()
			}
		})

	if len(conditions) > 1:
		json["filter"] = {
			"$and": conditions
		}
	elif len(conditions) == 1:
		json["filter"] = conditions[0]


@statistic_blueprint.route("/csv", methods=["GET"])
@utils.authentication_required
def view_csv() -> typing.Tuple[flask.Response, int]:
	json = {
		"order": {
			"by": "creation_timestamp",
			"asc": False
		},
		"limit": 512
	}

	transform_json_with_time_conditions(json)

	statistics = utils.request(
		flask.g.session,
		"query",
		"statistics",
		json=json
	).json()

	csv_output = io.StringIO()
	csv_writer = csv.writer(
		csv_output,
		quoting=csv.QUOTE_MINIMAL
	)

	csv_writer.writerow([
		flask_babel.gettext("Time"),
		flask_babel.gettext("Users"),
		flask_babel.gettext("Threads"),
		flask_babel.gettext("Posts")
	])

	if len(statistics) != 0:
		for statistic in reversed(statistics):
			csv_writer.writerow([
				statistic["creation_timestamp"],
				statistic["user_count"],
				statistic["thread_count"],
				statistic["post_count"]
			])

	response = flask.make_response(csv_output.getvalue())
	response.headers["Content-type"] = "text/csv"
	response.headers["Content-Disposition"] = (
		"attachment; filename="
		+ flask_babel.gettext("statistics")
		+ ".csv"
	)

	return response, http.client.OK


@statistic_blueprint.route("/image", methods=["GET"])
@utils.authentication_required
def view_image() -> typing.Tuple[flask.Response, int]:
	json = {
		"order": {
			"by": "creation_timestamp",
			"asc": False
		},
		"limit": 512
	}

	transform_json_with_time_conditions(json)

	statistics = utils.request(
		flask.g.session,
		"query",
		"statistics",
		json=json
	).json()

	if len(statistics) == 0:
		total_width, total_height = (1000, 500)

		image = PIL.Image.new(
			mode="RGB",
			size=(total_width, total_height),
			color=(255, 255, 255)
		)

		font = PIL.ImageFont.truetype(
			os.path.join(
				os.path.dirname(
					os.path.dirname(__file__)
				),
				"static",
				"fonts",
				"Roboto-Regular.ttf"
			),
			size=50
		)

		text = flask_babel.gettext("No statistics available")
		text_width, text_height = font.getsize(text)

		image_draw = PIL.ImageDraw.Draw(image)

		image_draw.text(
			(
				(total_width - text_width)/2,
				(total_height - text_height)/2
			),
			text,
			fill=(0, 0, 0),
			font=font
		)

		image_io = io.BytesIO()

		image.save(image_io, "PNG")

		image_io.seek(0)

		return flask.send_file(
			image_io,
			mimetype="image/png",
			as_attachment=False,
			download_name=(
				flask_babel.gettext("statistics")
				+ ".png"
			)
		), http.client.OK

	users_x = []
	threads_x = []
	posts_x = []

	y = []

	for statistic in reversed(statistics):
		users_x.append(statistic["user_count"])
		threads_x.append(statistic["thread_count"])
		posts_x.append(statistic["post_count"])

		y.append(
			datetime.datetime.fromisoformat(statistic["creation_timestamp"])
		)

	figure, axis = matplotlib.pyplot.subplots(figsize=(10, 5))

	axis.set_title(
		flask_babel.gettext("Statistics - ")
		+ flask_babel.format_datetime(
			datetime.datetime.fromisoformat(
				statistics[len(statistics) - 1]["creation_timestamp"]
			)
		)
		+ flask_babel.gettext(" to ")
		+ flask_babel.format_datetime(
			datetime.datetime.fromisoformat(
				statistics[0]["creation_timestamp"]
			)
		)
	)
	axis.set_xlabel(flask_babel.gettext("Time"))
	axis.set_ylabel(flask_babel.gettext("Item count"))

	axis.plot(
		y,
		users_x,
		label=flask_babel.gettext("Users")
	)
	axis.plot(
		y,
		threads_x,
		label=flask_babel.gettext("Threads")
	)
	axis.plot(
		y,
		posts_x,
		label=flask_babel.gettext("Posts")
	)

	figure.autofmt_xdate()

	axis.legend()

	image_buffer = io.BytesIO()
	figure.savefig(image_buffer, format="png")
	image_buffer.seek(0)

	return flask.send_file(
		image_buffer,
		mimetype="image/png",
		as_attachment=False,
		download_name=(
			flask_babel.gettext("statistics")
			+ ".png"
		)
	), http.client.OK
