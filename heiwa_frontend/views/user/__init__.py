import http.client
import io
import email
import typing
import uuid

import flask

from heiwa_frontend import utils

__all__ = [
	"user_blueprint",
	"view",
	"view_avatar",
	"view_public_key"
]


user_blueprint = flask.Blueprint(
	"user",
	__name__,
)


@user_blueprint.route("/users/<uuid:id_>/avatar", methods=["GET"])
@utils.authentication_required
def view_avatar(id_: uuid.UUID) -> typing.Union[
	flask.Response,
	typing.Tuple[flask.Response, int]
]:
	avatar = utils.request(
		flask.g.session,
		"get",
		f"users/{id_}/avatar"
	)

	if not avatar.headers["Content-Type"].startswith("image/"):
		# Not an image, very likely :data:`None`
		return flask.redirect(
			flask.url_for(
				"static",
				filename="images/default_avatar.svg"
			)
		)

	return flask.send_file(
		io.BytesIO(avatar.content),
		mimetype=avatar.headers["Content-Type"],
		as_attachment=False,
		download_name=avatar.headers["Content-Disposition"].split("filename=")[1],
		last_modified=email.utils.parsedate_to_datetime(
			avatar.headers["Last-Modified"]
		).timestamp()
	), http.client.OK


@user_blueprint.route("/users/<uuid:id_>", methods=["GET"])
@utils.authentication_required
@utils.full_view()
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	user = utils.request(
		flask.g.session,
		"get",
		f"users/{id_}"
	).json()

	raw_user = user.copy()

	user["$allowed_actions"] = utils.request(
		flask.g.session,
		"get",
		f"users/{user['id']}/allowed-actions"
	).json()

	if user["thread_count"] != 0:
		user["$threads"] = utils.request(
			flask.g.session,
			"query",
			"threads",
			json={
				"limit": 3,
				"filter": {
					"$equals": {"user_id": user["id"]}
				}
			}
		).json()

	if user["post_count"] != 0:
		user["$posts"] = utils.request(
			flask.g.session,
			"query",
			"posts",
			json={
				"limit": 5,
				"filter": {
					"$equals": {"user_id": user["id"]}
				}
			}
		).json()

	return flask.render_template(
		"user_view.html",
		user=user,
		raw_user=raw_user
	), http.client.OK


@user_blueprint.route("/users/<uuid:id_>/public-key", methods=["GET"])
@user_blueprint.route("/self/public-key", methods=["GET"])
@utils.authentication_required
@utils.full_view()
def view_public_key(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	if id_ is None:
		id_ = flask.g.user.id

	user = utils.request(
		flask.g.session,
		"get",
		f"users/{id_}"
	).json()

	return flask.render_template(
		"user_view_public_key.html",
		user=user
	), http.client.OK
