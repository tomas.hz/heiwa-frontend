import http.client
import io
import email
import typing

import flask

from heiwa_frontend import utils

__all__ = [
	"meta_blueprint",
	"view_icon"
]


meta_blueprint = flask.Blueprint(
	"meta",
	__name__,
	url_prefix="/meta"
)


@meta_blueprint.route("/icon", methods=["GET"])
def view_icon() -> typing.Union[
	flask.Response,
	typing.Tuple[flask.Response, int]
]:
	icon = utils.request(
		flask.g.session,
		"get",
		"meta/icon"
	)

	if icon.headers["Content-Type"] == "application/ld+json":
		return flask.redirect(
			flask.url_for(
				"static",
				filename="images/icon.png"
			)
		)

	return flask.send_file(
		io.BytesIO(icon.content),
		mimetype=icon.headers["Content-Type"],
		as_attachment=False,
		download_name=icon.headers["Content-Disposition"].split("filename=")[1],
		last_modified=email.utils.parsedate_to_datetime(
			icon.headers["Last-Modified"]
		).timestamp()
	), http.client.OK
