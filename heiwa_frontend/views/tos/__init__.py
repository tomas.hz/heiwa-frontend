import http.client
import typing

import flask

from heiwa_frontend import utils

__all__ = [
	"tos_blueprint",
	"view"
]


tos_blueprint = flask.Blueprint(
	"tos",
	__name__,
	url_prefix="/tos"
)


@tos_blueprint.route("", methods=["GET"])
@utils.full_view(anonymous=True)
def view() -> typing.Tuple[flask.Response, int]:
	tos = utils.request(
		flask.g.session,
		"get",
		"meta/info",
		ignore_token=True
	).json()["tos"]

	return flask.render_template(
		"tos.html",
		tos=tos
	), http.client.OK
