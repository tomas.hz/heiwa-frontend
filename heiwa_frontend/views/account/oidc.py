import urllib

import flask
import werkzeug.exceptions

from heiwa_frontend import utils

__all__ = [
	"account_oidc_blueprint",
	"authenticate",
	"login"
]


account_oidc_blueprint = flask.Blueprint(
	"oidc",
	__name__,
	url_prefix="/oidc"
)


@account_oidc_blueprint.route("/login", methods=["GET"])
def login() -> flask.Response:
	with flask.g.session.cache_disabled():
		service = urllib.parse.quote(
			flask.request.args.get("service", ""),
			safe=""
		)

		if service == "":
			raise werkzeug.exceptions.NotFound

		account_info = utils.request(
			flask.g.session,
			"get",
			f"oidc/{service}/login",
			json={
				"redirect_url": flask.url_for(
					"account.oidc.authenticate",
					_external=True
				)
			}
		).json()

		return flask.redirect(account_info["url"])


@account_oidc_blueprint.route("/authenticate", methods=["GET"])
def authenticate() -> flask.Response:
	service = urllib.parse.quote(
		flask.request.args.get("service", ""),
		safe=""
	)

	if service == "":
		raise werkzeug.exceptions.NotFound

	backend_config = utils.request(
		flask.g.session,
		"get",
		"meta/config"
	).json()

	with flask.g.session.cache_disabled():
		code = flask.request.args.get("code", "")
		state = flask.request.args.get("state", "")

		if len(code) == 0 or len(state) == 0:
			raise werkzeug.exceptions.BadRequest

		token = utils.request(
			flask.g.session,
			"post",
			f"oidc/{service}/authenticate",
			json={
				"code": code,
				"state": state,
				"redirect_url": flask.url_for(
					"account.oidc.authenticate",
					_external=True
				)
			}
		).json()

		response = flask.make_response(
			flask.redirect(
				flask.url_for("forum.list_")
			)
		)

		response.set_cookie(
			"token",
			token,
			max_age=backend_config["jwt_expires_after"]
		)

		return response
