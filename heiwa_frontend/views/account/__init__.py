import http.client
import typing

import flask
import flask_babel
import flask_wtf
import wtforms
import wtforms.validators

from heiwa_frontend import utils

from .local import account_local_blueprint
from .oidc import account_oidc_blueprint

__all__ = [
	"LoginForm",
	"RegistrationForm",
	"account_blueprint",
	"account_local_blueprint",
	"account_oidc_blueprint",
	"login",
	"logout",
	"register"
]


class RegistrationForm(flask_wtf.FlaskForm):
	email = wtforms.StringField(
		flask_babel.lazy_gettext("Email address"),
		[
			wtforms.validators.InputRequired(),
			wtforms.validators.Email()
		],
		render_kw={
			"type": "email"
		}
	)
	password = wtforms.PasswordField(
		flask_babel.lazy_gettext("Password"),
		[
			wtforms.validators.InputRequired()
		]
	)
	submit = wtforms.SubmitField(
		flask_babel.lazy_gettext("Register")
	)


class LoginForm(RegistrationForm):
	submit = wtforms.SubmitField(
		flask_babel.lazy_gettext("Log in")
	)


account_blueprint = flask.Blueprint(
	"account",
	__name__,
	url_prefix="/account"
)

for blueprint in (
	account_oidc_blueprint,
	account_local_blueprint
):
	account_blueprint.register_blueprint(blueprint)


@account_blueprint.route("/login", methods=["GET"])
@utils.authentication_required
@utils.full_view()
def login() -> flask.Response:
	form = LoginForm()

	if form.validate_on_submit():
		return "TODO"

	return flask.render_template(
		"login.html",
		openid_services=utils.request(
			flask.g.session,
			"get",
			"oidc"
		).json(),
		form=form
	), http.client.OK


@account_blueprint.route("/logout", methods=["GET"])
@utils.authentication_required
@utils.full_view()
def logout() -> flask.Response:
	response = flask.make_response(
		flask.render_template("logout.html")
	)

	response.delete_cookie("token")

	return response, http.client.OK


@account_blueprint.route("/register", methods=["GET", "POST"])
@utils.full_view(anonymous=True)
def register() -> typing.Union[flask.Response, int]:
	form = RegistrationForm()

	if form.validate_on_submit():
		return "TODO"

	return flask.render_template(
		"register.html",
		form=form
	)
