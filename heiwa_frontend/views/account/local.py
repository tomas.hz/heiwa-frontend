import typing

import flask

from heiwa_frontend import utils

__all__ = [
	"account_local_blueprint",
	"verify"
]


account_local_blueprint = flask.Blueprint(
	"local",
	__name__,
	url_prefix="/local"
)


@account_local_blueprint.route("/verify")
@utils.full_view(anonymous=True)
def verify() -> typing.Union[flask.Response, int]:
	return "TODO"
