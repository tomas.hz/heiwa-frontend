import http.client
import typing
import uuid

import flask

from heiwa_frontend import utils

__all__ = [
	"forum_blueprint",
	"list_",
	"parse_category",
	"parse_forum",
	"parse_thread",
	"view"
]


forum_blueprint = flask.Blueprint(
	"forum",
	__name__,
	url_prefix="/forums"
)


def parse_category(
	category: typing.Dict,
	parse_forums: bool = True
) -> typing.List[typing.Dict]:
	forums = utils.request(
		flask.g.session,
		"query",
		"forums",
		json={
			"order": {
				"by": "order",
				"asc": True
			},
			"limit": 512,
			"filter": {
				"$equals": {"category_id": category["id"]}
			}
		}
	).json()

	raw_forums = forums.copy()

	category["forums"] = [
		parse_forum(forum) if parse_forums else forum
		for forum in forums
	]

	return raw_forums


def parse_forum(forum: typing.Dict) -> typing.Dict:
	# Don't need any allowed actions here. The editing will be done through
	# an admin panel (TODO), and subscriptions will happen on the thread
	# listing.

	latest_threads = utils.request(
		flask.g.session,
		"query",
		"threads",
		json={
			"order": {
				"by": "creation_timestamp",
				"asc": False
			},
			"limit": 1,
			"filter": {
				"$equals": {"forum_id": forum["id"]}
			}
		}
	).json()

	if len(latest_threads) == 0:
		forum["$latest_thread"] = None
		return forum

	latest_thread = latest_threads[0]

	forum["$latest_thread"] = latest_thread

	forum["$latest_thread"]["$author"] = utils.request(
		flask.g.session,
		"get",
		f"users/{latest_thread['user_id']}"
	).json()

	return forum


def parse_thread(thread: typing.Dict) -> typing.Dict:
	thread["$author"] = utils.request(
		flask.g.session,
		"get",
		f"users/{thread['user_id']}"
	).json()

	thread["$allowed_actions"] = utils.request(
		flask.g.session,
		"get",
		f"threads/{thread['id']}/allowed-actions"
	).json()

	latest_posts = utils.request(
		flask.g.session,
		"query",
		"posts",
		json={
			"order": {
				"by": "creation_timestamp",
				"asc": False
			},
			"limit": 1,
			"filter": {
				"$equals": {"thread_id": thread["id"]}
			}
		}
	).json()

	if len(latest_posts) == 0:
		thread["$latest_post"] = None
		return thread

	thread["$latest_post"] = latest_posts[0]

	thread["$latest_post"]["$author"] = utils.request(
		flask.g.session,
		"get",
		f"users/{thread['$latest_post']['user_id']}"
	).json()

	return thread


@forum_blueprint.route("", methods=["GET"])
@utils.authentication_required
@utils.full_view()
def list_() -> typing.Tuple[flask.Response, int]:
	futures = []

	uncategorized_forums = utils.request(
		flask.g.session,
		"query",
		"forums",
		json={
			"order": {
				"by": "order",
				"asc": True
			},
			"limit": 512,
			"filter": {
				"$and": [
					{"$equals": {"parent_forum_id": None}},
					{"$equals": {"category_id": None}}
				]
			}
		}
	).json()

	raw_forums_and_categories = uncategorized_forums.copy()

	if not len(uncategorized_forums) == 0:
		futures += [
			flask.current_app.executor.submit(
				parse_forum,
				forum
			)
			for forum in uncategorized_forums
		]

	categories = utils.request(
		flask.g.session,
		"query",
		"categories",
		json={
			"order": {
				"by": "order",
				"asc": True
			},
			"limit": 512,
			"filter": {
				"$equals": {"forum_id": None}
			}
		}
	).json()

	raw_forums_and_categories += categories

	if not len(categories) == 0:
		futures += [
			flask.current_app.executor.submit(
				parse_category,
				category
			)
			for category in categories
		]

	utils.handle_futures(futures)

	for future in futures:
		raw_forums_and_categories.append(future.result())

	return flask.render_template(
		"forum_list.html",
		categories=categories,
		uncategorized_forums=uncategorized_forums,
		raw_forums_and_categories=raw_forums_and_categories
	), http.client.OK


@forum_blueprint.route("/<uuid:id_>", methods=["GET", "POST"])
@utils.authentication_required
@utils.full_view()
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	disable_forum_subscription_cache = False

	if flask.request.method == "POST":
		subscribe = flask.request.args.get("subscribe")

		if subscribe is not None:
			if subscribe == "true":
				method = "put"
			elif subscribe == "false":
				method = "delete"

			utils.request(
				flask.g.session,
				method,
				f"forums/{id_}/subscription"
			)

		disable_forum_subscription_cache = True

	futures = []

	forum = utils.request(
		flask.g.session,
		"get",
		f"forums/{id_}"
	).json()

	raw_forum_and_threads = [forum.copy()]

	forum["$allowed_actions"] = utils.request(
		flask.g.session,
		"get",
		f"forums/{id_}/allowed-actions"
	).json()

	if "edit_subscription" in forum["$allowed_actions"]:
		def get_subscription() -> dict:
			return utils.request(
				flask.g.session,
				"get",
				f"forums{id_}/subscription"
			).json()

		if disable_forum_subscription_cache:
			with flask.g.session.cache_disabled():
				forum["$is_subscribed"] = get_subscription()
		else:
			forum["$is_subscribed"] = get_subscription()

	forum["$child_categories"] = utils.request(
		flask.g.session,
		"query",
		"categories",
		json={
			"order": {
				"by": "order",
				"asc": True
			},
			"limit": 512,
			"filter": {
				"$equals": {"forum_id": forum["id"]}
			}
		}
	).json()

	forum["$child_forums_uncategorized"] = utils.request(
		flask.g.session,
		"query",
		"forums",
		json={
			"order": {
				"by": "order",
				"asc": True
			},
			"limit": 512,
			"filter": {
				"$equals": {"parent_forum_id": forum["id"]}
			}
		}
	).json()

	if not len(forum["$child_categories"]) == 0:
		futures += [
			flask.current_app.executor.submit(
				parse_category,
				category,
				parse_forums=False
			)
			for category in forum["$child_categories"]
		]

	forum["$threads"] = []

	threads = utils.request(
		flask.g.session,
		"query",
		"threads",
		json={
			"limit": 512,
			"filter": {
				"$and": [
					{"$equals": {"forum_id": forum["id"]}},
					{"$equals": {"is_pinned": True}}
				]
			}
		}
	).json()

	raw_forum_and_threads += threads

	# Pinned threads first
	forum["$threads"] += [
		parse_thread(thread)
		for thread in threads
	]

	forum["$threads"] += [
		parse_thread(thread)
		for thread in (
			utils.request(
				flask.g.session,
				"query",
				"threads",
				json={
					"limit": 25,
					"filter": {
						"$and": [
							{"$equals": {"forum_id": forum["id"]}},
							{"$equals": {"is_pinned": False}}
						]
					}
				}
			).json()
		)
	]

	if len(forum["$threads"]) != 0:
		futures += [
			flask.current_app.executor.submit(
				parse_thread,
				thread
			)
			for thread in forum["$threads"]
		]

	if len(futures) != 0:
		utils.handle_futures(futures)

	return flask.render_template(
		"forum_view.html",
		forum=forum,
		raw_forum_and_threads=raw_forum_and_threads
	), http.client.OK
